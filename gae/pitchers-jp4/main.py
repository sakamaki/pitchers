#!/usr/bin/env python
# -*- coding:utf-8 -*-
#
# __copyright__ = "Copyright 2011, inc."
# __author__ = 'sakamaki'
#
from google.appengine.api import channel
from google.appengine.ext import webapp
from google.appengine.ext.webapp import util
from google.appengine.ext import db
from google.appengine.ext.webapp import template
import logging
import uuid
import os

logging.getLogger().setLevel(logging.INFO)
my_uuid = 0
#投資家情報
class InvestorInfo (db.Model):
    db_inv_uuid = db.StringProperty(required=False)
    #db_name = db.StringProperty(required=False)

class Pair (db.Model):
    db_inv_uuid = db.StringProperty(required=False)
    db_pre_uuid = db.StringProperty(required=False)

#今のところ使ってない
class MainHandler(webapp.RequestHandler):
    def get(self):
        self.response.out.write('''
            hoge
        ''')

#トークン取得
class CreateTokenHandler(webapp.RequestHandler):
    def get(self):
        logging.info(my_uuid)
        channel_token = channel.create_channel(my_uuid)
        logging.info(channel_token)
        # TODO:uuid find or insert
        self.response.out.write( channel_token )

#投資家からの開始容認通知受信
class AcceptPitchHandler(webapp.RequestHandler):
    def post(self, *args):
        for data in Pair.all():
            inv_uuid = data.db_inv_uuid
            pre_uuid = data.db_pre_uuid

        logging.info(pre_uuid)
        json_result = '{"accept_pitch": "accept", "inv_uuid": "%s"}' % inv_uuid
        try:
            # PUSH to Presentator
            channel.send_message( pre_uuid, json_result)
        except Exception:
            logging.error('AcceptPitchHandler channel send message error')
            pass

#プレゼンテーターからのオファー受信
class OfferHandler(webapp.RequestHandler):
    def post(self, *args):
        pair_uuids = Pair()
        #TODO:プロトタイプは1pairしか許容しない
        for data in pair_uuids.all():
            data.delete()

        inv_uuid = self.request.body

        pair_uuids.db_inv_uuid = inv_uuid
        pair_uuids.db_pre_uuid = my_uuid

        logging.info(my_uuid)
        logging.info(inv_uuid)

        pair_uuids.put()

        json_result = '{"offer": "offer", "pre_uuid": "%s"}' % my_uuid
        try:
            # PUSH to Investor
            channel.send_message( inv_uuid, json_result)
        except Exception:
            logging.error('ReceiveOfferHandler channel send message error')
        pass

#投資家リスト取得
class InvestorListHandler(webapp.RequestHandler):
    def get(self):
        for data in InvestorInfo.all():
            inv_uuid = data.db_inv_uuid

        template_values = {
            'inv_uuid': inv_uuid,
        }
        path = os.path.join(os.path.dirname(__file__), 'popup-inv.html')
        self.response.out.write(template.render(path, template_values))

    def post(self):
        inv_info = InvestorInfo()
        inv_uuid = self.request.body
        #デモは投資家１件の登録のみにする
        for data in inv_info.all():
            data.delete()

        #DBに保存
        logging.info(inv_uuid)
        inv_info.db_inv_uuid = inv_uuid
        inv_info.put()


class UuidHandler(webapp.RequestHandler):
    def get(self, *args):
        global my_uuid
        my_uuid = str(uuid.uuid4())
        logging.info(my_uuid)
        self.response.out.write(my_uuid)

#投資家がHangoutに入る歳に通知される
class StartUpHandler(webapp.RequestHandler):
    def post(self):
        for data in Pair.all():
            pre_uuid = data.db_pre_uuid
            inv_uuid = data.db_inv_uuid
            logging.info(pre_uuid)
            logging.info(data.db_inv_uuid)

        json_result = '{"startup": "startup"}'

        try:
            #両者に開始をPUSH
            channel.send_message( pre_uuid, json_result)
            channel.send_message( inv_uuid, json_result)
        except Exception:
            logging.error('AcceptPitchHandler channel send message error')
            pass


class ContinueCountHandler(webapp.RequestHandler):
    def post(self):
        for data in Pair.all():
            inv_uuid = data.db_inv_uuid
            pre_uuid = data.db_pre_uuid
            logging.info(pre_uuid)
            logging.info(data.db_inv_uuid)

        json_result = '{"contcount": "contcount"}'

        try:
            channel.send_message( inv_uuid, json_result)
            channel.send_message( pre_uuid, json_result)
        except Exception:
            logging.error('AcceptPitchHandler channel send message error')
            pass

def main():
    application = webapp.WSGIApplication([('/', MainHandler),
        ('/uuid', UuidHandler),
        ('/channel-token', CreateTokenHandler),
        ('/investor-list', InvestorListHandler),
        ('/offer', OfferHandler),
        ('/startup', StartUpHandler),
        ('/contcount', ContinueCountHandler),
        ('/accept-pitch', AcceptPitchHandler)],
                                             debug=True)
    util.run_wsgi_app(application)

if __name__ == '__main__':
    main()
