/**
 * Created by PyCharm.
 * User: sakamaki
 * Date: 11/11/24
 * Time: 21:27
 * To change this template use File | Settings | File Templates.
 */
var base_url = 'http://pitchers-jp3.appspot.com/';
var channel;
var socket;
var token;
var my_uuid;
var investorFlg = false;
var presentatorFlg = false;
var pre_uuid = null;
var inv_uuid = null;

function sendMessage(path, message) {
    var xhr = new XMLHttpRequest();
    var url = base_url + path;
    xhr.open('POST', url, true);
    xhr.send(message);
}

function channelStart () {
    channel = new goog.appengine.Channel(token);
    socket = channel.open();
    socket.onopen = function(){
        //接続確立時の処理
    };
    socket.onmessage = function(event){
        var json_data = JSON.parse(event.data);
        if ('accept' == json_data.accept_pitch) {
            //開始容認通知を受け取ったときの処理
            //contentscriptにカウントダウン開始を通知する
            //デスクトップ通知
            var notification = webkitNotifications.createHTMLNotification('notify_presentator.html');
            notification.show();
            setTimeout(function(){
                notification.cancel();
            },5000);

        } else if ('offer' == json_data.offer) {
            //デスクトップ通知
            var BG=this;
            BG.message='オファーを受信しました。';//独自プロパティを付与
            var notification = webkitNotifications.createHTMLNotification('notify_investor.html');
            notification.show();
            setTimeout(function(){
                notification.cancel();
            },300000);

            pre_uuid = json_data.pre_uuid;
            inv_uuid = my_uuid;
            presentatorFlg = false;
            investorFlg = true;
        } else if ('startup' == json_data.startup) {
            //カウントダウン開始　デスクトップ通知
            var notification = webkitNotifications.createHTMLNotification('notify_startup.html');
            notification.show();
            setTimeout(function(){
                notification.cancel();
            },3000);
            //カウントダウン開始
            chrome.tabs.getSelected(null, function(tab) {
                chrome.tabs.sendRequest(tab.id, {startup: "startup"}, function(response) {});
            });
        } else if ('contcount' == json_data.contcount) {
            //続行カウントダウン開始　デスクトップ通知
            var notification = webkitNotifications.createHTMLNotification('notify_continue.html');
            notification.show();
            setTimeout(function(){
                notification.cancel();
            },3000);
            //content scriptへカウントダウン開始
            chrome.tabs.getSelected(null, function(tab) {
                chrome.tabs.sendRequest(tab.id, {cont: "cont"}, function(response) {});
            });
        }
    };
    socket.onerror = function(event){
        //エラーを受け取ったときの処理
    };
    socket.onclose = function(){
        //接続が終了したときの処理
    };
}


function getToken(callback) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function(data) {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                data = xhr.responseText;
                callback(data);
            } else {
                callback(null);
            }
        }
    };
    var url = base_url + 'channel-token';
    xhr.open('GET', url, false);
    xhr.send();
}
function callbackToken(data) {
    token = data;
    setTimeout("getToken(callbackToken)", 7000000);//２時間でトークンの有効期限が切れるため
    channelStart();
}

//WebページやContentScriptからのメッセージ受付
chrome.extension.onRequest.addListener(
    function(request, sender, sendResponse) {
        if (request.select_inv_uuid!= null) {
            //投資家選択時処理
            presentatorFlg = true;
            investorFlg = false;
            pre_uuid = my_uuid;
            inv_uuid = request.select_inv_uuid;
            //sendMessage('/offer', inv_uuid);
        } else if (request.offer != null) {
            //オファー送信
            if (inv_uuid != null) {
                sendMessage('/offer', inv_uuid);
            }
        } else if (request.accept_pitch != null) {
            //開始容認通知
            sendMessage('/accept-pitch', pre_uuid);

        } else if (request.release_modal != null) {
            sendMessage('/contcount', inv_uuid);
            //ContentScriptにモーダルウィンドウを閉じるよう依頼する。
            chrome.tabs.getSelected(null, function(tab) {
                chrome.tabs.sendRequest(tab.id, {release_modal: "release_modal"}, function(response) {
                    console.log(response.farewell);
                });
            });

        } else if (request.reg_inv != null) {
            //投資家UUID登録
            sendResponse({inv_uuid: my_uuid});

        } else if (request.startup != null) {
            //種別（プレゼン／投資家）の返却
            if (investorFlg) {
                sendResponse({kind: 'inv'});
            } else if (presentatorFlg) {
                sendResponse({kind: 'pre'});
            } else {
                sendResponse({kind: 'none'});
            }
        } else if (request.notify_startup != null) {
            //プレゼンテーターに開始を通知
            sendMessage('/startup', pre_uuid);
            sendResponse();
        } else if (request.get_uuid != null) {
            //自身のUUIDを取得
            getUUID(callbackUUID);
        }
    }
);

function getUUID(callback) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function(data) {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                //var data = JSON.parse(xhr.responseText);
                var data = xhr.responseText;
                callback(data);
            } else {
                callback(null);
            }
        }
    };
    // Note that any URL fetched here must be matched by a permission in
    // the manifest.json file!
    var url = base_url + '/uuid';
    xhr.open('GET', url, true);
    xhr.send();
}

function callbackUUID(data) {
    my_uuid = data;
    alert('my_uuid' + ':' + my_uuid);
    getToken(callbackToken);
}

//getUUID(callbackUUID);
