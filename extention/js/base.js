/**
 * Created by PyCharm.
 * User: sakamaki
 * Date: 11/11/22
 * Time: 20:01
 * To change this template use File | Settings | File Templates.
 */

var pitchers = pitchers ? pitchers : new Object();
pitchers.version = "0.0.0";
var start_time = '00:90';
var presen_kind;

function addSelectBox() {
    var pit_base_div = document.createElement("div");
    pit_base_div.id = "pitchers_outer";
    pit_base_div.style.cssText = 'border:1px solid #cbc6bd;background:#e8e4db;-moz-border-radius:5px;-webkit-border-radius:5px;font-size:14px;padding:7px;padding-bottom:30;height:100px;top:0px;';
    pit_base_div.top = 0;

    var logoDiv = document.createElement("div");
    logoDiv.id = "pitchers_title";
    logoDiv.style.cssText = 'float:left';
    logoDiv.innerHTML = "<image style=\"vertical-align:-5px;\" src=\"" + chrome.extension.getURL("img/logo.png") + "\"> ver." + pitchers.version + " ";

    var counterDiv = document.createElement("div");
    counterDiv.id = 'timer';
    counterDiv.style.cssText = "float:right";

    pit_base_div.appendChild(logoDiv);
    pit_base_div.appendChild(counterDiv);

    document.body.insertBefore(pit_base_div, document.body.childNodes[0]);
}


function setupView() {

    startCountDown();

    //hangouts全体の表示領域を調整（領域を下にずらす）
    //TODO: まだ、うまく行ってない要調整。
    $("div.gcomm-ip-jp").css("top",function (currentTop) {
        return currentTop + 115;
    });

    $("div.talk_chat_widget").height(function (index, currentHeight) {
        return currentHeight - 115;
    });

    $("div.talk_chat_widget").css("top",function (currentTop) {
        return currentTop + 350;
    });
}

function startCountDown () {
    //カウンタの表示領域を設定
    $('div#timer').append('<div id="counter"></div>');
    $('div#timer').append('<div class="desc"></div>').css({ margin: '7px 3px' });
    $('.cntSeparator').css({
        'font-size': '54px',
        'margin': '10px 7px',
        'color': '#000'
    });

    //カウントダウンタイマのセットアップ
    $('div.desc').append('<div>残り時間</div>');
    $('div.desc').append('<div></div>');
    $('desc div').css({
        'float': 'left',
        'font-family': 'Arial',
        'width': '70px',
        'margin-right': '65px',
        'font-size': '13px',
        'font-weight': 'bold',
        'color': '#000'
    });

    // モーダルウィンドウのセットアップ
    $('div.gcomm-ip-jp').append('<div id="backLayer"></div>');
    $('div.gcomm-ip-jp').append('<div id="popLayer"></div>');
    var g_checkout = chrome.extension.getURL("button.html");
    $('div.#popLayer').append('<iframe src=' + g_checkout + ' ' + 'scrolling="yes" frameborder="0" style="border:none; overflow:hidden; width:100%; height:100%;" allowTransparency="true"></iframe>');


    var timeOutEvent;
    if (presen_kind == 'pre') {
        timeOutEvent = function(){
                $("div#backLayer").show();
                $("div#popLayer").show().css({
                    marginTop:"-"+$("div#popLayer").height()/2+"px" ,
                    marginLeft:"-"+$("div#popLayer").width()/2+"px"
                });
            }
    } else if (presen_kind == 'inv') {
        timeOutEvent = function(){}
    }
    //カウントダウン開始
    $('#counter').countdown({
        image: chrome.extension.getURL("img/digits.png"),
        startTime: start_time,
        format: 'mm:ss',
        timerEnd: timeOutEvent
    });
}

function releaseModalWindow () {
    $("div#backLayer").remove();
    $("div#popLayer").remove();
    $("div#counter").remove();
    $("div.desc").remove();
    startCountDown();
}

chrome.extension.onRequest.addListener(
    function(request, sender, sendResponse) {
        /*
        if (request.release_modal != null) {
            //モーダルウィンドウとじる
            //releaseModalWindow();
            //Backgroundへ続行通知

        } else
        */
        if (request.continuePresen != null) {
            //続行通知受信処理(投資家側が行う処理)
            releaseModalWindow('inv');
        } else if (request.startup != null) {
            setTimeout(setupView, 10000);
        } else if (request.cont != null) {
            releaseModalWindow();
        }
    }
);

function notifyStartup () {
    chrome.extension.sendRequest({notify_startup: "notify_startup"}, function(response) {});
}

function notifyOffer() {
    chrome.extension.sendRequest({offer: "offer"}, function(response) {});
}

function initialize () {
    //Backgroundページに自身がプレゼンテーターか？投資家か？確認
    chrome.extension.sendRequest({startup: "startup"}, function(response) {
        presen_kind = response.kind;
        //プレゼン開始(プレゼンテータ、投資家両方)
        if ('none' != presen_kind) {
            addSelectBox();
            if ('inv' == presen_kind) {
                //プレゼン開始通知を行う
                notifyStartup();
            } else if ('pre' == presen_kind) {
                //オファーを行う
                notifyOffer();

            }
        }
    });
}

initialize();

